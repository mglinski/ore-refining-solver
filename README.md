## Ore Refinign Solver

Uses the google Linear Equation solver engine to find the best quantities of ore to refine to hit a specific goal.

### Usage

```bash
python solve.py
```

#### Example Output

![](http://cl.ly/1V1N1H0Y0Z25/Image%202016-07-01%20at%2011.37.41%20PM.png)