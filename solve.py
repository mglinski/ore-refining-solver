from __future__ import print_function
from google.apputils import app
from ortools.linear_solver import pywraplp
from array import array
import math
import urllib, json
import locale
from sys import version_info


py3 = version_info[0] > 2 #creates boolean value for test that Python major version > 2

def main(_):
    locale.setlocale(locale.LC_ALL, '')

    # Instantiate a Glop solver, naming it SolveSimpleSystem.
    solver = pywraplp.Solver('SolveSimpleSystem', pywraplp.Solver.GLOP_LINEAR_PROGRAMMING)

    # Set to False to use hardcoded inputs
    if True:

        querystring = "How many units of {} do you need: "
        if py3:
            IsogenQty = int(input(querystring.format("Isogen")))
        else:
            IsogenQty = int(raw_input(querystring.format("Isogen")))

        if py3:
            MegacyteQty = int(input(querystring.format("Megacyte")))
        else:
            MegacyteQty = int(raw_input(querystring.format("Megacyte")))

        if py3:
            MexallonQty = int(input(querystring.format("Mexallon")))
        else:
            MexallonQty = int(raw_input(querystring.format("Mexallon")))

        if py3:
            NocxiumQty = int(input(querystring.format("Nocxium")))
        else:
            NocxiumQty = int(raw_input(querystring.format("Nocxium")))

        if py3:
            PyeriteQty = int(input(querystring.format("Pyerite")))
        else:
            PyeriteQty = int(raw_input(querystring.format("Pyerite")))

        if py3:
            TritaniumQty = int(input(querystring.format("Tritanium")))
        else:
            TritaniumQty = int(raw_input(querystring.format("Tritanium")))

        if py3:
            ZydrineQty = int(input(querystring.format("Zydrine")))
        else:
            ZydrineQty = int(raw_input(querystring.format("Zydrine")))

        print('Thanks!')
        print('')

    else:

        # Set The Amount of Each Mineral You Need
        IsogenQty = 20000
        MegacyteQty = 20000
        MexallonQty = 20000
        NocxiumQty = 20000
        PyeriteQty = 20000
        TritaniumQty = 20000
        ZydrineQty = 20000

    print('Calculating optimal ore...')

    # Set Ore Prices
    arkonorPrice = 478680  # Isk
    bistotPrice = 503537  # Isk
    crokitePrice = 471126  # Isk
    ochrePrice = 216429  # Isk
    gneissPrice = 167068  # Isk
    spodumainPrice = 471599  # Isk

    # DO NOT EDIT BELOW THIS LINE
    # DO NOT EDIT BELOW THIS LINE
    # DO NOT EDIT BELOW THIS LINE
    # UNLESS YOU KNOW WHAT YOUR DOING

    IsogenRefine = array('I')
    IsogenRefine.append(0)
    IsogenRefine.append(0)
    IsogenRefine.append(0)
    IsogenRefine.append(1411)
    IsogenRefine.append(265)
    IsogenRefine.append(0)

    MegacyteRefine = array('I')
    MegacyteRefine.append(281)
    MegacyteRefine.append(88)
    MegacyteRefine.append(0)
    MegacyteRefine.append(0)
    MegacyteRefine.append(0)
    MegacyteRefine.append(0)

    MexallonRefine = array('I')
    MexallonRefine.append(2205)
    MexallonRefine.append(0)
    MexallonRefine.append(0)
    MexallonRefine.append(0)
    MexallonRefine.append(2117)
    MexallonRefine.append(1852)

    NocxiumRefine = array('I')
    NocxiumRefine.append(0)
    NocxiumRefine.append(0)
    NocxiumRefine.append(670)
    NocxiumRefine.append(106)
    NocxiumRefine.append(0)
    NocxiumRefine.append(0)

    PyeriteRefine = array('I')
    PyeriteRefine.append(0)
    PyeriteRefine.append(10584)
    PyeriteRefine.append(0)
    PyeriteRefine.append(0)
    PyeriteRefine.append(1940)
    PyeriteRefine.append(10628)

    TritaniumRefine = array('I')
    TritaniumRefine.append(22000)
    TritaniumRefine.append(0)
    TritaniumRefine.append(18522)
    TritaniumRefine.append(8820)
    TritaniumRefine.append(0)
    TritaniumRefine.append(49392)

    ZydrineRefine = array('I')
    ZydrineRefine.append(0)
    ZydrineRefine.append(396)
    ZydrineRefine.append(118)
    ZydrineRefine.append(0)
    ZydrineRefine.append(0)
    ZydrineRefine.append(0)

    # Create the two variables in our system of equations,
    # and let them take on any value.
    a = solver.IntVar(0, 100000, 'a')
    b = solver.IntVar(0, 100000, 'b')
    c = solver.IntVar(0, 100000, 'c')
    o = solver.IntVar(0, 100000, 'o')
    g = solver.IntVar(0, 100000, 'g')
    s = solver.IntVar(0, 100000, 's')

    # Apply Refining Constraints To Each
    upperBoundsConstraint = 1000000000

    constraint1 = solver.Constraint(IsogenQty, upperBoundsConstraint)
    constraint1.SetCoefficient(a, IsogenRefine[0])
    constraint1.SetCoefficient(b, IsogenRefine[1])
    constraint1.SetCoefficient(c, IsogenRefine[2])
    constraint1.SetCoefficient(o, IsogenRefine[3])
    constraint1.SetCoefficient(g, IsogenRefine[4])
    constraint1.SetCoefficient(s, IsogenRefine[5])

    constraint2 = solver.Constraint(MegacyteQty, upperBoundsConstraint)
    constraint2.SetCoefficient(a, MegacyteRefine[0])
    constraint2.SetCoefficient(b, MegacyteRefine[1])
    constraint2.SetCoefficient(c, MegacyteRefine[2])
    constraint2.SetCoefficient(o, MegacyteRefine[3])
    constraint2.SetCoefficient(g, MegacyteRefine[4])
    constraint2.SetCoefficient(s, MegacyteRefine[5])

    constraint3 = solver.Constraint(MexallonQty, upperBoundsConstraint)
    constraint3.SetCoefficient(a, MexallonRefine[0])
    constraint3.SetCoefficient(b, MexallonRefine[1])
    constraint3.SetCoefficient(c, MexallonRefine[2])
    constraint3.SetCoefficient(o, MexallonRefine[3])
    constraint3.SetCoefficient(g, MexallonRefine[4])
    constraint3.SetCoefficient(s, MexallonRefine[5])

    constraint4 = solver.Constraint(NocxiumQty, upperBoundsConstraint)
    constraint4.SetCoefficient(a, NocxiumRefine[0])
    constraint4.SetCoefficient(b, NocxiumRefine[1])
    constraint4.SetCoefficient(c, NocxiumRefine[2])
    constraint4.SetCoefficient(o, NocxiumRefine[3])
    constraint4.SetCoefficient(g, NocxiumRefine[4])
    constraint4.SetCoefficient(s, NocxiumRefine[5])

    constraint5 = solver.Constraint(PyeriteQty, upperBoundsConstraint)
    constraint5.SetCoefficient(a, PyeriteRefine[0])
    constraint5.SetCoefficient(b, PyeriteRefine[1])
    constraint5.SetCoefficient(c, PyeriteRefine[2])
    constraint5.SetCoefficient(o, PyeriteRefine[3])
    constraint5.SetCoefficient(g, PyeriteRefine[4])
    constraint5.SetCoefficient(s, PyeriteRefine[5])

    constraint6 = solver.Constraint(TritaniumQty, upperBoundsConstraint)
    constraint6.SetCoefficient(a, TritaniumRefine[0])
    constraint6.SetCoefficient(b, TritaniumRefine[1])
    constraint6.SetCoefficient(c, TritaniumRefine[2])
    constraint6.SetCoefficient(o, TritaniumRefine[3])
    constraint6.SetCoefficient(g, TritaniumRefine[4])
    constraint6.SetCoefficient(s, TritaniumRefine[5])

    constraint7 = solver.Constraint(ZydrineQty, upperBoundsConstraint)
    constraint7.SetCoefficient(a, ZydrineRefine[0])
    constraint7.SetCoefficient(b, ZydrineRefine[1])
    constraint7.SetCoefficient(c, ZydrineRefine[2])
    constraint7.SetCoefficient(o, ZydrineRefine[3])
    constraint7.SetCoefficient(g, ZydrineRefine[4])
    constraint7.SetCoefficient(s, ZydrineRefine[5])

    # Define our objective: maximizing 3x + 4y.
    objective = solver.Objective()
    objective.SetCoefficient(a, arkonorPrice)
    objective.SetCoefficient(b, bistotPrice)
    objective.SetCoefficient(c, crokitePrice)
    objective.SetCoefficient(o, ochrePrice)
    objective.SetCoefficient(g, gneissPrice)
    objective.SetCoefficient(s, spodumainPrice)
    objective.SetMinimization()

    # Solve the system.
    solver.Solve()

    print('Done!')

    totalCost = (arkonorPrice * math.ceil(a.solution_value())) + (bistotPrice * math.ceil(b.solution_value())) + (
    crokitePrice * math.ceil(c.solution_value())) + (ochrePrice * math.ceil(o.solution_value())) + (
                gneissPrice * math.ceil(g.solution_value())) + (spodumainPrice * math.ceil(s.solution_value()))

    # The value of each variable in the solution.
    print('Needed Ore:')
    print('======================')
    print('5% Arkonor Ore   =', math.ceil(a.solution_value()), 'Units')
    print('5% Bistot Ore    =', math.ceil(b.solution_value()), 'Units')
    print('5% Crokite Ore   =', math.ceil(c.solution_value()), 'Units')
    print('5% Ochre Ore     =', math.ceil(o.solution_value()), 'Units')
    print('5% Gneiss Ore    =', math.ceil(g.solution_value()), 'Units')
    print('5% Spodumain Ore =', math.ceil(s.solution_value()), 'Units')
    print('----------------------')
    print('Total Cost')
    print("ISK {}".format(locale.format("%d", totalCost, grouping=True)))

if __name__ == '__main__':
    app.run()
